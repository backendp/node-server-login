const express = require("express");
const jwt = require("jsonwebtoken");

const app = express();
app.use(express.json());

app.get("/api", (req, res) => {
  res.json({ message: "Testing API" });
});

app.post("/login", (req, res) => {
  const user = {
    email: req.body.username,
    password: req.body.password,
  };
  // creation of token for the user
  if (user.email == "silvia@mail.com" && user.password == "1234") {
    jwt.sign(
      { user },
      "env_secret_key",
      /* {expiresIn: '32s'}, */ (err, token) => {
        res.json({
          token,
        });
      }
    );
  } else {
    res.sendStatus(403);
  }
});

app.post("/posts", verifyToken, (req, res) => {
  jwt.verify(req.token, "env_secret_key", (err, auth) => {
    if (err) {
      res.sendStatus(403);
    } else {
      res.json({
        message: "Post created",
        auth,
      });
    }
  });
});

// Authorization: Bearer <token>
function verifyToken(req, res, next) {
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    // access token
    const bearerToken = bearerHeader.split(" ")[1];
    req.token = bearerToken;
    next();
  } else {
    console.log("object");
    res.sendStatus(403);
  }
}

app.listen(3000, () => {
  console.log("Server running in port", 3000);
});
